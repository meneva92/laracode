<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get_rooms/{take?}/{skip?}','RoomController@get_all_record');
Route::get('get_room_types/{take?}/{skip?}','RoomTypeController@get_all_record');
Route::get('get_room_capacities/{take?}/{skip?}','RoomCapacityController@get_all_record');

