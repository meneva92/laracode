<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('get_hotels','HotelController@get_all_record');
Route::get('get_rooms','RoomController@get_all_record');
Route::get('get_room_types','RoomTypeController@get_all_record');
Route::get('get_room_capacities','RoomCapacityController@get_all_record');
Route::get('get_pricelist','PricelistController@get_all_record');

Route::post('save_hotels','HotelController@create');
Route::post('save_rooms','RoomController@create');
Route::post('save_room_types','RoomTypeController@create');
Route::post('save_room_capacities','RoomCapacityController@create');

Route::get('remove_hotels/{id}','HotelController@destroy');
Route::get('remove_rooms/{id}','RoomController@destroy');
Route::get('remove_room_types/{id}','RoomTypeController@destroy');
Route::get('remove_room_capacities/{id}','RoomCapacityController@destroy');

Route::resource('hotel', 'HotelController');
Route::resource('room', 'RoomController');
Route::resource('room_type', 'RoomTypeController');
Route::resource('room_capacity', 'RoomCapacityController');



