<?php

use Illuminate\Database\Seeder;
use App\Room_type;
class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         /* truncate all records first from room_type table */
         Room_type::truncate();

         //$faker = \Faker\Factory::create();
 
         // create a few room types in our database:(e.g. Deluxe, Standard,Premium)
         $type_array = ['Deluxe','Standard','Premium'];
         for ($i = 0; $i < sizeof($type_array); $i++) {
            Room_type::create([
                'type_name' => $type_array[$i]
            ]);
         }
    }
}
