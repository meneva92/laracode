<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Room_capacity;

class RoomCapacitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        /* truncate all records first from room_capacicty table */
        Room_capacity::truncate();

        //$faker = \Faker\Factory::create();

        // create a few room capacities in our database:(e.g. Single, Double, Family)
        $capa_array = ['Single Room','Double Room','Family Room'];
        for ($i = 0; $i < sizeof($capa_array); $i++) {
            Room_capacity::create([
                'capacity_name' => $capa_array[$i]
            ]);
        }
    }
}
