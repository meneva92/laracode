<?php

use Illuminate\Database\Seeder;
use App\Room;
class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         /* truncate all records first from room table */
         Room::truncate();

         //$faker = \Faker\Factory::create();
 
         $ar_1 = [array('name'=>'A1','type_id'=>1,'capacity_id'=>2), array('name'=>'A2','type_id'=>1,'capacity_id'=>1),array('name'=>'A3','type_id'=>2,'capacity_id'=>3),array('name'=>'B1','type_id'=>1,'capacity_id'=>2), array('name'=>'B2','type_id'=>1,'capacity_id'=>1),array('name'=>'B3','type_id'=>1,'capacity_id'=>2),array('name'=>'C1','type_id'=>1,'capacity_id'=>1), array('name'=>'C2','type_id'=>1,'capacity_id'=>2),array('name'=>'C3','type_id'=>2,'capacity_id'=>3),array('name'=>'D1','type_id'=>2,'capacity_id'=>1), array('name'=>'D2','type_id'=>3,'capacity_id'=>2),array('name'=>'D3','type_id'=>3,'capacity_id'=>1)];
         foreach($ar_1 as $ar){
             Room::create([
                 'name'=>$ar['name'],
                 'type_id'=>$ar['type_id'],
                 'capacity_id'=>$ar['capacity_id']
                ]);
         }
    }
}
