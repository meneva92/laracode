<?php

use Illuminate\Database\Seeder;
use App\Booking;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* truncate all records first from bookings table */
        Booking::truncate();

        //$faker = \Faker\Factory::create();

        // create a few booking entries in database\
        /* $begin = new DateTime('2019-06-20');
        $end = new DateTime('2019-07-01');
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $r=1;
        foreach ($period as $dt) {
            echo $dt->format("Y-m-d H:i:s\n");
            Booking::create([
                'room_id' => $r,
                'start_date'=>
            ]);
            $r++;
        } */
    }
}
