@extends('layouts.app')
@section('content')
<div class="container" ng-app="roomApp" ng-controller="roomCtrl">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div kendo-grid="grid" options="mainGridOptions" k-rebind="mainGridOptions" id="grid1"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- add/update modal -->
    <div id="add_edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Room Details</h4>
            </div>
                <div class="modal-body">
                    <form action="#" id="form" name="form" class="form-horizontal">
                        <div class="form-group required">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-9">
                                <input name="name" required  class="form-control" type="text" ng-model="insert_update.name"> 
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Room Type</label>
                            <div class="col-md-9" style="width:50%;">
                            <select kendo-drop-down-list 
                                id='room_type'
                                k-data-text-field="'type_name'"
                                k-data-value-field="'id'"
                                k-data-source="all_room_types",
                                ng-model="insert_update.type_id"
                                k-filter="'contains'"
                                k-option-label="'Select Room Type'"
                                style="width: 100%">
                            </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Room Capacity</label>
                            <div class="col-md-9" style="width:50%;">
                            <select kendo-drop-down-list 
                                id='room_capacity'
                                k-data-text-field="'capacity_name'"
                                k-data-value-field="'id'"
                                k-data-source="all_room_capacities",
                                ng-model="insert_update.capacity_id"
                                k-filter="'contains'"
                                k-option-label="'Select Room Capacity'"
                                style="width: 100%">
                            </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <!--  <div class="form-group">
                            <label class="control-label col-md-3">Details </label>
                            <div class="col-md-9" style="width:50%;">
                                <textarea name="details" id="details" ng-model="insert_update.details" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div> -->
                    </form>
                    </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button ng-disabled="form.$invalid" type="button" class="btn btn-default pull-right" data-dismiss="modal" ng-click="save_data()">Save</button>
            </div>
            </div>
        </div>
    </div>
    <!-- //add-update modal -->
</div>

<script>
    var app = angular.module('roomApp', ['kendo.directives']);
    app.controller('roomCtrl', function ($scope, $http) {  
        $scope.insert_update = {};
        $scope.all_room_types = [];
        $scope.all_room_capacities = [];
        $scope.loadGrid = function(){
        $scope.mainGridOptions = {
            dataSource: {
                transport: {
                    read:  {
                        url: "get_rooms",
                        dataType: "json",
                        type:"GET"
                    },
                    
                    parameterMap: function(options,operation) {
                        var take =(options.take == undefined)?"all":options.take;
                        return {take: take,skip:options.skip,page:options.page,pageSize:options.pageSize};
                    }
                },
                serverPaging: true,
                serverFiltering: false,
                pageSize: 50,
                schema: {
                    data: "records",
                    total: "total",
                    model: {
                        id: "id"
                    }
                } 
            },
            serverSorting: true,
            sortable: true,
            selectable: true,
            pageable: {
                refresh: true,
                pageSizes: [50,100,150,200,"All"],
                buttonCount: 5,
                messages: {
                        itemsPerPage: "Items per Page",
                        display: "{0}-{1}" + " of  {2} Record" ,
                        empty: "No Record Found",
                        allPages: "All"
                    }
            },
            filterable: { mode: "row"},
            toolbar: [{
                text: "New Room",
                template: "<input type='button' class='k-button k-i-plus' value='New Room' ng-click='add_edit_data()' />"
            }],
            columns: [
                    { title: "Name", field: "name", width: "20%", filterable: { cell: { operator: "contains", showOperators: false } } },
                    { title: "Room Type", field: "type_name", width: "20%", filterable: false },
                    { title: "Room Capacity", field: "capacity_name", width: "20%", filterable: { cell: { operator: "contains", showOperators: false } } },
                    
                    { title: "Action", field: "status", width: "20%", filterable: false,
                        template: " <button type='button' class='btn btn-outline green' ng-click='view_pubs(dataItem.id)'><i class='fa fa-eye'></i></button> <button type='button' class='btn btn-outline green' ng-click='add_edit_data(dataItem)'><i class='fa fa-edit'></i></button> <button type='button' class='btn btn-outline green' ng-click='delete_data(dataItem.id)'><i class='fa fa-trash'></i></button>",
                    }
                ]
            
        };
    };

    $scope.loadGrid();

    $scope.add_edit_data = function(item=null){
        //start_spinner();
        $http.get("api/get_room_types/All/0").then(function(res){
            $scope.all_room_types = res.data.records;
        });
        $http.get("api/get_room_capacities/All/0").then(function(res){
            $scope.all_room_capacities = res.data.records;
        });      
        $scope.insert_update = {};
        if(item!=undefined || item!=null){
            $scope.is_new = 0;
            $scope.insert_update.id = item.id;
            $scope.insert_update.name = item.name;
            $scope.insert_update.type_id = item.type_id;
            $scope.insert_update.capacity_id = item.capacity_id; 
        }
        else{
            $scope.is_new = 1;
            $scope.insert_update = {};
            //stop_spinner();
        }
        
        $("#add_edit_modal").modal('show');
        console.log($scope.insert_update);
        console.log('insert/update: '+$scope.is_new);
    }

    $scope.save_data = function(){
        if($scope.is_new==1){
            $scope.insert_update.id=0;
        }
        $http.post("/save_rooms",$scope.insert_update).then(function(res){
            if(res.data.status==1){
                show_success_msg('Data has been saved successfully');
                $scope.loadGrid();
            }
            else{
                show_err_msg('some error occured !!!');
            }
        });
    }
    $scope.delete_data = function(id){
        $http.get("/remove_rooms/"+id).then(function(res){
            if(res.data==1){
                show_success_msg('Data has been deleted successfully');
                $scope.loadGrid();
            }
            else{
                show_err_msg('some error occured !!!');
            }
        });
    }

 });
</script>

@endsection