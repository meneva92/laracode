@extends('layouts.app')
@section('content')
<div class="container" ng-app="roomApp" ng-controller="roomCtrl">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Hotel</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div kendo-grid="grid" options="mainGridOptions" k-rebind="mainGridOptions" id="grid1"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- add/update modal -->
    <div id="add_edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Hotel Details</h4>
            </div>
                <div class="modal-body">
                    <form action="#" id="form" name="form" class="form-horizontal">
                        <div class="form-group required">
                            <label class="control-label col-md-3">Name</label>
                            <div class="col-md-9">
                                <input name="name" required  class="form-control" type="text" ng-model="insert_update.name"> 
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Address </label>
                            <div class="col-md-9" style="width:50%;">
                                <textarea name="address" id="address" ng-model="insert_update.address" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">City</label>
                            <div class="col-md-9" style="width:50%;">
                            <select kendo-drop-down-list 
                                id='city'
                                k-data-text-field="'name'"
                                k-data-value-field="'name'"
                                k-data-source="all_city_list",
                                ng-model="insert_update.city"
                                k-filter="'contains'"
                                k-option-label="'Select City'"
                                style="width: 100%">
                            </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">State</label>
                            <div class="col-md-9" style="width:50%;">
                            <select kendo-drop-down-list 
                                id='state'
                                k-data-text-field="'name'"
                                k-data-value-field="'name'"
                                k-data-source="all_state_list",
                                ng-model="insert_update.state"
                                k-filter="'contains'"
                                k-option-label="'Select State'"
                                style="width: 100%">
                            </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Country</label>
                            <div class="col-md-9" style="width:50%;">
                            <select kendo-drop-down-list 
                                id='country'
                                k-data-text-field="'name'"
                                k-data-value-field="'name'"
                                k-data-source="all_country_list",
                                ng-model="insert_update.country"
                                k-filter="'contains'"
                                k-option-label="'Select Country'"
                                style="width: 100%">
                            </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="control-label col-md-3">Zip code</label>
                            <div class="col-md-9">
                                <input name="zipcode" required  class="form-control" type="text" ng-model="insert_update.zipcode"> 
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="control-label col-md-3">Phone</label>
                            <div class="col-md-9">
                                <input name="phone" required  class="form-control" type="text" ng-model="insert_update.phone"> 
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" required  class="form-control" type="text" ng-model="insert_update.email"> 
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </form>
                    </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button ng-disabled="form.$invalid" type="button" class="btn btn-default pull-right" data-dismiss="modal" ng-click="save_data()">Save</button>
            </div>
            </div>
        </div>
    </div>
    <!-- //add-update modal -->
</div>

<script>
    var app = angular.module('roomApp', ['kendo.directives']);
    app.controller('roomCtrl', function ($scope, $http) {  
        $scope.insert_update = {};
        $scope.all_room_types = [];
        $scope.all_room_capacities = [];
        $scope.loadGrid = function(){
        $scope.mainGridOptions = {
            dataSource: {
                transport: {
                    read:  {
                        url: "get_hotels",
                        dataType: "json",
                        type:"GET"
                    },
                    
                    parameterMap: function(options,operation) {
                        var take =(options.take == undefined)?"all":options.take;
                        return {take: take,skip:options.skip,page:options.page,pageSize:options.pageSize};
                    }
                },
                serverPaging: true,
                serverFiltering: false,
                pageSize: 50,
                schema: {
                    data: "records",
                    total: "total",
                    model: {
                        id: "id"
                    }
                } 
            },
            serverSorting: true,
            sortable: true,
            selectable: true,
            pageable: {
                refresh: true,
                pageSizes: [50,100,150,200,"All"],
                buttonCount: 5,
                messages: {
                        itemsPerPage: "Items per Page",
                        display: "{0}-{1}" + " of  {2} Record" ,
                        empty: "No Record Found",
                        allPages: "All"
                    }
            },
            filterable: { mode: "row"},
            toolbar: [{
                text: "New Hotel",
                template: "<input type='button' class='k-button k-i-plus' value='New Hotel' ng-click='add_edit_data()' />"
            }],
            columns: [
                    { title: "Name", field: "name", width: "20%", filterable: { cell: { operator: "contains", showOperators: false } } },
                    { title: "City", field: "city", width: "20%", filterable: false },
                    { title: "State", field: "state", width: "20%", filterable: false },                   
                    { title: "Country", field: "country", width: "20%", filterable: false },                   
                    { title: "Zip Code", field: "zipcode", width: "20%", filterable: false },                   
                    { title: "Phone", field: "phone", width: "20%", filterable: false },                   
                    { title: "Email", field: "email", width: "20%", filterable: false },                   
                    { title: "Action", field: "status", width: "20%", filterable: false,
                        template: "<button type='button' class='btn btn-outline green' ng-click='add_edit_data(dataItem)'><i class='fa fa-edit'></i></button> <button type='button' class='btn btn-outline green' ng-click='delete_data(dataItem.id)'><i class='fa fa-trash'></i></button>",
                    }
                ]
            
        };
    };

    $scope.loadGrid();

    $scope.add_edit_data = function(item=null){
        //start_spinner();
        $all_city_list = [{'name':'Dhaka'},{'name':'Chittagong'},{'name':'Rajshahi'}];      
        $all_state_list = [{'name':'DHK'},{'name':'CTG'},{'name':'RJS'}];      
        $all_country_list = [{'name':'Bangladesh'},{'name':'India'},{'name':'China'}];      
        $scope.insert_update = {};
        if(item!=undefined || item!=null){
            $scope.is_new = 0;
            $scope.insert_update.id = item.id;
            $scope.insert_update.name = item.name;
            $scope.insert_update.city = item.city;
            $scope.insert_update.state = item.state;
            $scope.insert_update.country = item.country;
            $scope.insert_update.zipcode = item.zipcode;
            $scope.insert_update.phone = item.phone;
            $scope.insert_update.email = item.email; 
        }
        else{
            $scope.is_new = 1;
            $scope.insert_update = {};
            //stop_spinner();
        }
        
        $("#add_edit_modal").modal('show');
        console.log($scope.insert_update);
    }

    $scope.save_data = function(){
        if($scope.is_new==1){
            $scope.insert_update.id=0;
        }
        $http.post("/save_hotels",$scope.insert_update).then(function(res){
            if(res.data.status==1){
                show_success_msg('Data has been saved successfully');
                $scope.loadGrid();
            }
            else{
                show_err_msg('some error occured !!!');
            }
        });
    }
    $scope.delete_data = function(id){
        $http.get("/remove_hotels/"+id).then(function(res){
            if(res.data==1){
                show_success_msg('Data has been deleted successfully');
                $scope.loadGrid();
            }
            else{
                show_err_msg('some error occured !!!');
            }
        });
    }

 });
</script>

@endsection