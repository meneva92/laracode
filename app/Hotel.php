<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Hotel extends Model
{
    //protected $fillable = ['name','address','city'];
    public static function get_all_hotels($take=null,$skip=null){
        $take = ($take==null)?'All':$take;
        $skip = ($skip==null)?0:$skip;
        if($take == 'All'){
            $limit = " ORDER BY id DESC";
        }else{
            $limit = " ORDER BY id DESC LIMIT ".$skip.', '.$take;
        }
        $total = Room::count();
        $all_records = DB::select("select * from hotels ".$limit);
        return ['total'=>$total, 'records'=>$all_records];
    }

    public static function saveData($data){
		//return $data['name'];
		if($data['id'] ==0){
			$data['name'] = DB::getPdo()->quote($data['name']);
			$save_sql = DB::insert("INSERT INTO hotels(name,address,city,state,country,zipcode,phone,email) VALUES(".$data['name'].",".$data['address'].", ".$data['city'].", ".$data['state'].", ".$data['country'].", ".$data['zipcode'].", ".$data['phone'].", ".$data['email'].")");
		}else{
			$save_sql = DB::table('hotels')->where('id', $data['id'])->update(['name' => $data['name'],'address' => $data['address'],'city' => $data['city'],'state' => $data['state'],'country' => $data['country'],'zipcode' => $data['zipcode'],'phone' => $data['phone'],'email' => $data['email']]);
		}
		if($save_sql){
			/* if(isset($data->new_docs) && sizeof($data->new_docs)>0){
				if(self::saveTaskDocs($task_id, $data->new_docs)){
					$return_data['status'] = 1;
				}else{
					$this->task_db->rollback();
					$return_data['status'] = 0;
				}
			}else{
				$return_data['status'] = 1;
			} */
			$return_data['status'] = 1;
		}else{
			$return_data['status'] = 0;
		}
		return $return_data;
	}

	public static function removeData($id){
		$remove_sql = DB::table('hotels')->where('id', $id)->delete();
		if($remove_sql){
			return  1;
		}
		else{
			return 0;
		}
	}
}
