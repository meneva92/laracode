<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Room_type extends Model
{
    protected $fillable = ['type_name'];

    public static function get_all_room_types($take=null,$skip=null){
        $take = ($take==null)?'All':$take;
        $skip = ($skip==null)?0:$skip;
        if($take == 'All'){
            $limit = " ORDER BY id DESC";
        }else{
            $limit = " ORDER BY id DESC LIMIT ".$skip.', '.$take;
        }
        $total = Room_type::count();
        $all_records = DB::select("select id, type_name from room_types ".$limit);
        return ['total'=>$total, 'records'=>$all_records];
    }

    public static function saveData($data){
		//return $data['name'];
		if($data['id'] ==0){
			$data['type_name'] = DB::getPdo()->quote($data['type_name']);
			$save_sql = DB::insert("INSERT INTO room_types(type_name) VALUES(".$data['type_name'].")");
		}else{
			$save_sql = DB::table('room_types')->where('id', $data['id'])->update(['type_name' => $data['type_name']]);
		}
		if($save_sql){
			$return_data['status'] = 1;
		}else{
			$return_data['status'] = 0;
		}
		return $return_data;
	}

	public static function removeData($id){
		$remove_sql = DB::table('room_types')->where('id', $id)->delete();
		if($remove_sql){
			return  1;
		}
		else{
			return 0;
		}
	}
}
