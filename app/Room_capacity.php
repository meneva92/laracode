<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Room_capacity extends Model
{
    protected $fillable = ['capacity_name'];
    public static function get_all_room_capacities($take=null,$skip=null){
        $take = ($take==null)?'All':$take;
        $skip = ($skip==null)?0:$skip;
        if($take == 'All'){
            $limit = " ORDER BY id DESC";
        }else{
            $limit = " ORDER BY id DESC LIMIT ".$skip.', '.$take;
        }
        $total = Room_capacity::count();
        $all_records = DB::select("select id, capacity_name from room_capacities ".$limit);
        return ['total'=>$total, 'records'=>$all_records];
    }

    public static function saveData($data){
		//return $data['name'];
		if($data['id'] ==0){
			$data['capacity_name'] = DB::getPdo()->quote($data['capacity_name']);
			$save_sql = DB::insert("INSERT INTO room_capacities(capacity_name) VALUES(".$data['capacity_name'].")");
		}else{
			$save_sql = DB::table('room_capacities')->where('id', $data['id'])->update(['capacity_name' => $data['capacity_name']]);
		}
		if($save_sql){
			$return_data['status'] = 1;
		}else{
			$return_data['status'] = 0;
		}
		return $return_data;
	}

	public static function removeData($id){
		$remove_sql = DB::table('room_capacities')->where('id', $id)->delete();
		if($remove_sql){
			return  1;
		}
		else{
			return 0;
		}
	}
}
