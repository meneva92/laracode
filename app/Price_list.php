<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Price_list extends Model
{
    protected $fillable = ['price_type','room_type','capacity_type','start_date','end_date','price'];
    public static function get_all_pricelist($take=null,$skip=null){
        $take = ($take==null)?'All':$take;
        $skip = ($skip==null)?0:$skip;
        if($take == 'All'){
            $limit = " ORDER BY id DESC";
        }else{
            $limit = " ORDER BY id DESC LIMIT ".$skip.', '.$take;
        }
        $total = Price_list::count();
        $all_records = DB::select("select id, (CASE WHEN price_type=1 THEN 'Regular' WHEN price_type=2 THEN 'Dynamic' END) as price_type_name, room_type,capacity_type,price,start_date,end_date from price_lists ".$limit);
        return ['total'=>$total, 'records'=>$all_records];
    }
}
