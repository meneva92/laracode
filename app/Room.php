<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Room extends Model
{
    protected $fillable = ['name','type_id','capacity_id'];
    public static function get_all_rooms($take=null,$skip=null){
        $take = ($take==null)?'All':$take;
        $skip = ($skip==null)?0:$skip;
        if($take == 'All'){
            $limit = " ORDER BY id DESC";
        }else{
            $limit = " ORDER BY id DESC LIMIT ".$skip.', '.$take;
        }
        $total = Room::count();
        $all_records = DB::select("select id, name, (select type_name from room_types where id=rooms.type_id) as type_name, type_id, (select capacity_name from room_capacities where id=rooms.capacity_id) as capacity_name, capacity_id from rooms ".$limit);
        return ['total'=>$total, 'records'=>$all_records];
    }

    public static function saveData($data){
		//return $data['name'];
		if($data['id'] ==0){
			$data['name'] = DB::getPdo()->quote($data['name']);
			$save_sql = DB::insert("INSERT INTO rooms(name,type_id,capacity_id) VALUES(".$data['name'].",".$data['type_id'].", ".$data['capacity_id'].")");
		}else{
			$save_sql = DB::table('rooms')->where('id', $data['id'])->update(['name' => $data['name'],'type_id' => $data['type_id'],'capacity_id' => $data['capacity_id']]);
		}
		if($save_sql){
			/* if(isset($data->new_docs) && sizeof($data->new_docs)>0){
				if(self::saveTaskDocs($task_id, $data->new_docs)){
					$return_data['status'] = 1;
				}else{
					$this->task_db->rollback();
					$return_data['status'] = 0;
				}
			}else{
				$return_data['status'] = 1;
			} */
			$return_data['status'] = 1;
		}else{
			$return_data['status'] = 0;
		}
		return $return_data;
	}

	public static function removeData($id){
		$remove_sql = DB::table('rooms')->where('id', $id)->delete();
		if($remove_sql){
			return  1;
		}
		else{
			return 0;
		}
	}
}
